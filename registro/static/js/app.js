﻿
$("#mostrar").click(function () {
    $("#formulario").show();
})
$("#ocultar").click(function () {
    $("#formulario").hide()
})
function limpiar() {
	document.getElementById("nombre").value = "";
	document.getElementById("run").value = "";
	document.getElementById("contrasenia").value = "";
	document.getElementById("fechanaci").value = "";
	document.getElementById("correo").value = "";
	document.getElementById("celular").value = "";
	document.getElementById("regiones").value = "";
	document.getElementById("comunas").value = "";
	document.getElementById("tipoCasa").value = "";
}
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
$('.carousel').carousel()

$(function () {
    $("#formulario").validate({
        rules: {
            nombre:{
                required: true,
                minlength: 5    
            },
            run:{
                required: true
			},
			contrasenia:{
				required: true,
			},
			fechanaci:{
				required: true,
			},
			correo: {
				required: true
			},
			celular:{
				required: true
			},
			regiones:{
				required: true
			},
			comunas:{
				required: true
			},
			tipoCasa:{
				required: true
			}
			
        },
        messages: {
            nombre: {
                required: "Debe ingresar su nombre completo",
                minlength: "Debe contener al menos 5 caracteres"
            },
            run: {
                required: "Debe ingresar rut valido",
                run: "Debe ingresar un correo con formato 11222333-4"
			},
			contrasenia:{
				required: "Ingrese contraseña"
			},
			fechanaci:{
				required: "Debe ingresar fecha nacimiento valida",
				max: "La fecha no debe ser mayor a 31-12-2001"
			},
			correo: {
				required: "Debe ingresar su correo",
				email: "Debe ingresar un correo con formato abc@abc.cl"
			},
			celular:{
				required: "Debe ingrese numero celular",
				maxlength: "Debe ingresar 9 digitos"
			},
			regiones:{
				required: "Seleccione Región"
			},
			comunas:{
				required: "Seleccione Comuna"
			},
			tipoCasa:{
				required: "Seleccione tipo casa"
			}


			
        }
    })
})

$(function () {
    $("#formulario-login").validate({
        rules: {
            correo:{
                required: true,   
            },
            contrasenia:{
                required: true
			}
        },
        messages: {
            correo: {
                required: "Debe ingresar su correo",
				email: "Debe ingresar un correo con formato abc@abc.cl"
            },
            contrasenia: {
                required: "Ingrese contraseña",
			}
        }
    })
})





$(document).ready(function(){
	$('#celular').mask('9 99 99 99 99');
	$('.mixed').mask('00.000.000-A');
	$('.date').mask('00/00/0000');
	$('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
});

function limpiar_fa() {
	document.getElementById("nombre").value = "";
	document.getElementById("raza").value = "";
	document.getElementById("foto").value = "";
	document.getElementById("estado").value = "";
	document.getElementById("descripcion").value = "";
}



function limpiarForm() {
	document.getElementById("rut").value = "";
	document.getElementById("clave").value = "";
	document.getElementById("nombre").value = "";
	document.getElementById("apellidoMaterno").value = "";
	document.getElementById("apellidoPaterno").value = "";
	document.getElementById("email").value = "";
	document.getElementById("telefono").value = "";
	document.getElementById("direccion").value = "";
	document.getElementById("nacionalidad").value = "";
	document.getElementById("genero").value = "";
	document.getElementById("fechaNacimiento").value = "";
	document.getElementById("prevision").value = "";
}
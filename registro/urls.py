from django.urls import path,include
from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.contrib import admin
from django.views.generic import TemplateView

urlpatterns = [
    
    path('',views.index,name="index"),


    path('index',views.index, name="index"),
    path('registro/',views.registro, name="registro"),
    path('registro/login',views.login, name="login"),
    path('registro/index',views.index, name="index"),
     path('registro/login_iniciar',views.login_iniciar,name="login_iniciar"),


     path('cerrarsession',views.cerrar_session,name="cerrar_session"),


     path('listarCliente/', views.listarCliente, name="listarCliente"),
     path('listarCliente/administrarCliente/<int:id>', views.administrarCliente, name="administrarCliente"),
     path('listarCliente/modificar_cliente/<int:id>', views.modificar_cliente, name="modificar_cliente"),
     path('listarCliente/eliminar_cliente/<int:id>', views.eliminar_cliente, name="eliminar_cliente"),




    path('registro/crearusu',views.crearusu , name="crearusu"),
    path('registro/buscar/<int:id>',views.buscar, name="buscar"),
    path('registro/editar/<int:id>',views.editar, name="editar"),
    path('registro/editado/<int:id>',views.editado, name="editado"),
    path('registro/eliminar/<int:id>',views.eliminar, name="eliminar")
]
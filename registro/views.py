from django.shortcuts import render
from django.http import HttpResponse
from .models import Persona
from .models import Objeto
from django.shortcuts import redirect

# Importar usuario
from django.contrib.auth.models import User
# Sistema de autenticación
from django.contrib.auth import authenticate, logout, login as auth_login
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required
# Create your views here.

def index(request):
    usuario = request.session.get('usuario', None)
    return render(request,'index.html',{'name': 'Registro de personas', 'usuario': usuario, 'mostrar': Objeto.objects.all()})

def registro(request):
    return render(request,'registrousuario.html',{})

def login(request):
    return render(request,'login.html')

    # cerrar_session
@login_required(login_url='login')
def cerrar_session(request):
    del request.session['usuario']
    logout(request)
    return redirect('index')

def login_iniciar(request):
    correo = request.POST.get('correo', '')
    contrasenia = request.POST.get('contrasenia', '')
    user = authenticate(username=correo, password=contrasenia)
    if user is not None:
        auth_login(request, user)
        request.session['usuario'] = user.first_name
        return redirect('index')
    else:
        return render(request, 'login.html', {'error': 'Usuario y/o contraseña incorrectos'})

def crearusu(request):
    nombre= request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    edad = request.POST.get('edad',0)
    correo = request.POST.get('correo', '')
    contrasenia = request.POST.get('contrasenia', '')
    objeto = Objeto(nombre=nombre,apellido=apellido,edad=edad,correo=correo,contrasenia=contrasenia)
    objeto.save()
    user = User.objects.create_user(username=correo, password=contrasenia,
                                    email=correo, first_name=nombre)
    user.save()
    return render(request, 'mensajeregistro.html', {'mensaje': 'Registro Exitoso'})

    return HttpResponse('nombre : '+nombre+" apellido : "+apellido )


def listarCliente(request):
    usuario = request.session.get('usuario', None)
    return render(request, 'listarCliente.html', {'name': 'Registro de personas', 'usuario': usuario,'clientes': Objeto.objects.all()})



def modificar_cliente(request, id):
    nombre = request.POST.get('nombre', '')
    apellido = request.POST.get('apellido', '')
    edad = request.POST.get('edad',0)
    correo = request.POST.get('correo', '')
    contrasenia = request.POST.get('contrasenia', '')
    clientes = Objeto.objects.get(pk=id)
    clientes.nombre = nombre
    clientes.apellido = apellido
    clientes.edad = edad
    clientes.correo = correo
    clientes.contrasenia = contrasenia
    clientes.save()
    return render(request, 'mensajeeditar.html', {'mensaje': 'Usuario Modificado Exitosamente'})
    return redirect('listarCliente')


def administrarCliente(request, id):
    clientes = Objeto.objects.get(id=id)
    usuario = request.session.get('usuario', None)
    print(clientes)
    return render(request, 'administrarCliente.html', {'clientes': clientes}) 

def eliminar_cliente(request, id):
    clientes = Objeto.objects.get(pk=id)
    clientes.delete()
    return redirect('listarCliente')
    return HttpResponse("Registro Eliminado Exitosamente")








def buscar (request,id):
    persona = Persona.objects.get(pk=id)
    return HttpResponse('nombre : '+persona.nombre+" apellido : "+persona.apellido)

def editar (request,id):
    persona = Persona.objects.get(pk=id)
    return render(request,'editar.html',{'persona':persona})

def editado (request,id):
    persona = Persona.objects.get(pk=id)
    nombre= request.POST.get('nombre','')
    apellido = request.POST.get('apellido','')
    edad = request.POST.get('edad',0)
    persona.nombre = nombre
    persona.apellido = apellido
    persona.edad = edad
    persona.save()
    return HttpResponse('nombre : '+persona.nombre+" apellido : "+persona.apellido)

def eliminar (request,id):
    persona = Persona.objects.get(pk=id)
    persona.delete()
    return HttpResponse("persona eliminada")
    


    
